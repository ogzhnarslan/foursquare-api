import { combineReducers } from 'redux';

import category from './category';
import venues from './venues';
import trend from './trend';

export default combineReducers({
	category,
	venues,
	trend
})