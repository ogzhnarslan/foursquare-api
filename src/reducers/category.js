import { FETCH_CATEGORY, FETCH_CATEGORY_PENDING, FETCH_CATEGORY_ERROR } from '../actions/category';

const initialSate = {
	fetching: true,
	category: []
}

export default (state = initialSate, action) => {
	switch (action.type) {
		case FETCH_CATEGORY_PENDING:
			return {
				...state,
				fetching: true
			}
		case FETCH_CATEGORY:
			return {
				...state,
				category: action.payload,
				fetching: false
			}
		case FETCH_CATEGORY_ERROR:
			return {
				...state,
				category: action.payload,
				fetching: false
			}
		default:
			return state;
	}
}