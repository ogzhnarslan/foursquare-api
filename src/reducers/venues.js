import { FETCH_VENUES, FETCH_VENUES_PENDING, FETCH_VENUES_ERROR } from '../actions/venues';

const initialSate = {
	fetching: true,
	venues: []
}

export default (state = initialSate, action) => {
	switch (action.type) {
		case FETCH_VENUES_PENDING:
			return {
				...state,
				fetching: true
			}
		case FETCH_VENUES:
			return {
				...state,
				venues: action.payload,
				fetching: false
			}
		case FETCH_VENUES_ERROR: 
			return {
				...state,
				venues: action.payload,
				fetching: false
			}
		default:
			return state;
	}
}