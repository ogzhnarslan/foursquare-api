import { FETCH_TREND, FETCH_TREND_PENDING, FETCH_TREND_ERROR } from '../actions/trend';

const initialSate = {
	fetching: true,
	trend: []
}

export default (state = initialSate, action) => {
	switch (action.type) {
		case FETCH_TREND_PENDING:
			return {
				...state,
				fetching: true
			}
		case FETCH_TREND:
			return {
				...state,
				trend: action.payload,
				fetching: false
			}
		case FETCH_TREND_ERROR: 
			return {
				...state,
				trend: action.payload,
				fetching: false
			}
		default:
			return state;
	}
}