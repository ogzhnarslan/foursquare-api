import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import { Row } from 'react-bootstrap';

import { fetchCategory } from './actions/category';

import 'bootstrap/dist/css/bootstrap.min.css';
import './_assets/css/App.scss';

import Home from './components/pages/Home';
import CategoryPage from './components/pages/CategoryPage';

import CategoryList from './components/lists/CategoryList';

import Background from './valetta.jpg';

class App extends Component {

	constructor(){
		super();
		this.onNavbar = this.onNavbar.bind(this);
	}

  static propTypes = {
    category: PropTypes.object.isRequired
  };

  state = {
    navbar: false
  }

  componentDidMount() {
    this.props.fetchCategory();
  }

  onNavbar() {
    this.setState({
      navbar: !this.state.navbar
    });
  }


  render() {
    var sectionStyle = {
      width: "100%",
      height: "100%",
      backgroundImage: `url(${Background})`,
      position: "fixed",
      left: "0px",
      top: "0px"
    };
    return (
      <BrowserRouter>
        <div className="App">
          <div className="valetta" style={ sectionStyle }></div>
          <div className="container-fluid">
            <Row>
              <button type="button" className={`navbar-toggle ${this.state.navbar}`} onClick={this.onNavbar}>
                <span className="sr-only">Toggle navigation</span>
                <span className="icon-bar"></span>
                <span className="icon-bar"></span>
                <span className="icon-bar"></span>
              </button>
              <CategoryList category={this.props.category} navbar={this.state.navbar}></CategoryList>
              <Switch>
                <Route exact path="/" component={Home} />
                <Route exact path='/category/:id' component={CategoryPage} />
              </Switch>
            </Row>
          </div>   
        </div>
      </BrowserRouter>
    );
  }
}

const mapStateToProps = ({category}) => {
  return {
    category
  }
};

const mapDispatchToProps = {
  fetchCategory
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
