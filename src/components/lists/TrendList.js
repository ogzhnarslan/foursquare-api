import React from 'react';
import PropTypes from 'prop-types';
import { Col, Media, Image } from 'react-bootstrap';
import { BarLoader } from 'react-spinners';
import Masonry from 'react-masonry-component';

const trendList = ({trend}) => {

  const emptyMessage = (
    <p>The are no trend yet.</p>
  );
  
  return (
    <div>
      <div className={ trend.fetching ? "col-md-12 loader" : "hide" }>
        <BarLoader
          sizeUnit={"px"}
          size={150}
          color={'#123abc'}
          loading={trend.fetching}
        />
      </div>
      <div className="clearfix"></div>
      <Masonry className={'venues-items'} elementType={'div'}>
      { 
        trend.length === 0 ? emptyMessage : trend.trend.map(trend =>
          <Col md={4} key={trend.id}>
            <Media>
              <Media.Left>
                <Image src={ trend.categories[0].icon.prefix + '64' + trend.categories[0].icon.suffix } circle />
              </Media.Left>
              <Media.Body>
                <Media.Heading>{ trend.name }</Media.Heading>
                <p>{ trend.categories[0].name }</p>
                <p>{ trend.location.address }{ trend.location.address !== undefined && trend.location.city !== undefined ? ',' : '' }{ trend.location.city }</p>
              </Media.Body>
            </Media>
          </Col>
        ) 
      }
      </Masonry>
    </div>
  );
};

trendList.propTypes = {
    trend: PropTypes.shape({
        trend: PropTypes.array.isRequired
  }).isRequired
}

export default trendList;