import React from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';
import { Col, Image } from 'react-bootstrap';
import { ClipLoader } from 'react-spinners';

const categoryList = ({category, navbar}) => {

  const emptyMessage = (
    <p>The are no category yet.</p>
  );

  const categoriesList = (
      <div className="nav">
        <NavLink key={1} to='/' exact className={ category.fetching ? "hide" : "clearfix animated fadeIn trend" } activeClassName="active">
          <span><i className="glyphicon glyphicon-star-empty"></i>Trend Olan Mekanlar</span>
        </NavLink>
        {
          category.category.map((category, index) => 
            <NavLink key={category.id} to={`/category/${category.id}`} className={`clearfix animated fadeIn delay${index}`} activeClassName="active">
              <Image src={ category.icon.prefix + '64' + category.icon.suffix } />
              <span>{ category.name }</span>
            </NavLink>
          )
        }
      </div>
  );

  return (
    <Col md={2} sm={3} className={`sidebar ${navbar}`}>
      	<ClipLoader
					sizeUnit={"px"}
					size={150}
					color={'#123abc'}
					loading={category.fetching}
				/>
        { category.length === 0 ? emptyMessage : categoriesList }
    </Col>
  );
};

categoryList.propTypes = {
  category: PropTypes.shape({
    category: PropTypes.array.isRequired
  }).isRequired
}

export default categoryList;
