import React from 'react';
import PropTypes from 'prop-types';
import { Col, Media, Image } from 'react-bootstrap';
import { BarLoader } from 'react-spinners';
import Masonry from 'react-masonry-component';

const venuesList = ({venues}) => {

  const emptyMessage = (
    <p>The are no category yet.</p>
  );
  
  return (
    <div>
      <div className={ venues.fetching ? "col-md-12 loader" : "hide" }>
        <BarLoader
          sizeUnit={"px"}
          size={150}
          color={'#123abc'}
          loading={venues.fetching}
        />
      </div>

      <div className="clearfix"></div>
      <Masonry className={'venues-items'} elementType={'div'}>
      { 
        venues.length === 0 ? emptyMessage : venues.venues.map(venues =>
          <Col md={4} key={venues.id}>
            <Media>
              <Media.Left>
                <Image src={ venues.categories[0].icon.prefix + '64' + venues.categories[0].icon.suffix } circle />
              </Media.Left>
              <Media.Body>
                <Media.Heading>{ venues.name }</Media.Heading>
                <p>{ venues.categories[0].name }</p>
                <p>{ venues.location.address }{ venues.location.address !== undefined && venues.location.city !== undefined ? ',' : '' }{ venues.location.city }</p>
              </Media.Body>
            </Media>
          </Col>
        ) 
      }
      </Masonry>
    </div>
  );
};

venuesList.propTypes = {
  venues: PropTypes.shape({
    venues: PropTypes.array.isRequired
  }).isRequired
}

export default venuesList;
