import React, {Component} from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Col } from 'react-bootstrap';
import { fetchTrend } from '../../actions/trend';

import TrendList from '../lists/TrendList';

class Home extends Component {

	static propTypes = {
		trend: PropTypes.object.isRequired
	};

	componentDidMount() {
		this.props.fetchTrend();
	}
	

	render() {
		return (
			<Col md={10} mdOffset={2} sm={9} smOffset={3} className="animated fadeIn main">
				<Col md={12}>
					<h1 className={ this.props.trend.fetching ? "hide" : "page-header animated fadeIn" }><span className="glyphicon glyphicon-star-empty"></span>Trend Olan Mekanlar</h1>
				</Col>
				<TrendList trend={this.props.trend} />
			</Col>
		);
	}
}

const mapStateToProps = ({trend}) => {
	return {
		trend
	}
  };
  
  const mapDispatchToProps = {
	fetchTrend
  }
  
  export default connect(mapStateToProps, mapDispatchToProps)(Home);