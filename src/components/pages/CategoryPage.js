import React, {Component} from 'react';
import { connect } from 'react-redux';
import { Col } from 'react-bootstrap';
import VenuesPage from './VenuesPage';
import { fetchVenues }  from '../../actions/venues';

class CategoryPage extends Component {
	componentDidMount() {
		const { match } = this.props;
		if (this.props.cat !== match.params.id) {
		  this.props.fetchVenues(match.params.id);
		}
	}

	componentWillReceiveProps(nextProps) {
		const { match } = this.props;
		if(nextProps !== undefined && nextProps.cat !== undefined) {
			if (nextProps.cat.id !== match.params.id) {
				this.props.fetchVenues(nextProps.cat.id);
			}
		}
	}

	render() {
		return (
			<Col md={10} mdOffset={2} sm={9} smOffset={3} className="animated fadeIn main">
				<VenuesPage cat={this.props.cat} venues={this.props.venues} />
			</Col>
		);
	}
}

const mapStateToProps = ({ category, venues }, props) => {
	return {
        venues,
		cat: category.category.find(item => item.id === props.match.params.id)
	}
};

const mapDispatchToProps = {
	fetchVenues
};

export default connect(mapStateToProps, mapDispatchToProps)(CategoryPage);