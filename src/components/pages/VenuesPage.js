import React, {Component} from 'react';
import { Col, Image } from 'react-bootstrap';
import VenuesList from '../lists/VenuesList';

class VenuesPage extends Component {
    state = {
		title: this.props.cat ? this.props.cat.name : '',
		prefix: '',
		suffix : '',
		venues: []
	}

	componentWillReceiveProps(nextProps) {
		if(nextProps.cat) {
			this.setState({
				title: nextProps.cat.name,
				prefix: nextProps.cat.icon.prefix,
				suffix: nextProps.cat.icon.suffix,
				venues: nextProps.venues
			});
		}
	}

	render() {
		return (
			<div>
				<Col md={12}>
					<h1 className={ this.props.venues.fetching ? "hide" : "page-header animated fadeIn" }>
						{ this.state.prefix ? <Image src={ this.state.prefix + 'bg_64' + this.state.suffix } /> : '' }
						{ this.state.title }
					</h1>
				</Col>
				<VenuesList venues={this.props.venues} />
			</div>
		);
	}
}
export default VenuesPage;