import axios from 'axios';

export const FETCH_CATEGORY = "FETCH_CATEGORY";
export const FETCH_CATEGORY_PENDING = "FETCH_CATEGORY_PENDING";
export const FETCH_CATEGORY_ERROR = "FETCH_CATEGORY_ERROR";

export function fetchCategory() {
    return dispatch => {
        axios.get('https://api.foursquare.com/v2/venues/categories', {
            params: {
                "v": 20171001,
                "client_id": "YVLW5UI5FRG4WK20UF4WO4HDXSYT4XIFUTD3GZOQHWNVVM2N",
                "client_secret": "C3B0MR3V1RJYJHZM1TD4UTXHZC1SJDP2DC4YBFRGLTFCJLVT"
            }
        })
        .then(res => res.data)
        .then(res => res.response)
        .then(res => dispatch({
            type: FETCH_CATEGORY,
            payload: res.categories
        }))
        .catch(res => dispatch({
            type: FETCH_CATEGORY_ERROR,
            payload: res.data
        }))
    }
}