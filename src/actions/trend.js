import axios from "axios";
export const FETCH_TREND = "FETCH_TREND";
export const FETCH_TREND_PENDING = "FETCH_TREND_PENDING";
export const FETCH_TREND_ERROR = "FETCH_TREND_ERROR";

export function fetchTrend() {
    return dispatch => {
        axios.get('https://api.foursquare.com/v2/venues/search', {
            params: {
                "near": "Valletta, Malta",
                "intent": "checkin",
                "limit": 20,
                "v": 20181001,
                "client_id": "YVLW5UI5FRG4WK20UF4WO4HDXSYT4XIFUTD3GZOQHWNVVM2N",
                "client_secret": "C3B0MR3V1RJYJHZM1TD4UTXHZC1SJDP2DC4YBFRGLTFCJLVT"
            }
        })
        .then(res => res.data)
        .then(res => res.response)
        .then(res => dispatch({
            type: FETCH_TREND,
            payload: res.venues
        }))
        .catch(res => dispatch({
            type: FETCH_TREND_ERROR,
            payload: res.data
        }))
    }
}