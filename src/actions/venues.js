import axios from "axios";
export const FETCH_VENUES = "FETCH_VENUES";
export const FETCH_VENUES_PENDING = "FETCH_VENUES_PENDING";
export const FETCH_VENUES_ERROR = "FETCH_VENUES_ERROR";

export function fetchVenues(id) {
    return dispatch => {
        axios.get('https://api.foursquare.com/v2/venues/search', {
            params: {
                "near": "Valletta, Malta",
                "categoryId": id,
                "v": 20181001,
                "client_id": "YVLW5UI5FRG4WK20UF4WO4HDXSYT4XIFUTD3GZOQHWNVVM2N",
                "client_secret": "C3B0MR3V1RJYJHZM1TD4UTXHZC1SJDP2DC4YBFRGLTFCJLVT"
            }
        })
        .then(res => res.data)
        .then(res => res.response)
        .then(res => dispatch({
            type: FETCH_VENUES,
            payload: res.venues
        }))
        .catch(res => dispatch({
            type: FETCH_VENUES_ERROR,
            payload: res.data
        }))
    }
}